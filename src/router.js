import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import GamesNotFinished from './views/GamesNotFinished.vue'
import Artwork from './views/Artwork.vue'

Vue.use(Router);

export default new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home
		},
		{
			path: '/games-not-finished',
			name: 'games-not-finished',
			component: GamesNotFinished
		},
		{
			path: '/art',
			name: 'art',
			component: Artwork
		}
	]
})
